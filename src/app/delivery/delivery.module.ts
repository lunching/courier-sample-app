import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PendingDeliveryComponent } from './pending-delivery/pending-delivery.component';
import { ProvidedDeliveryComponent } from './provided-delivery/provided-delivery.component';
import { DeliveryListComponent } from './delivery-list/delivery-list.component';

@NgModule({
  declarations: [PendingDeliveryComponent, ProvidedDeliveryComponent, DeliveryListComponent],
  imports: [
    CommonModule
  ],
  providers: []
})
export class DeliveryModule { }
