export class LunchSpot {
    public lunchSpotId: number;
    public name: string;
    public status: string;
    public address: string;
    public company: string;
    public deliveryTime: string;
}
