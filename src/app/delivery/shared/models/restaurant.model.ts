import { LunchSpot } from './lunch-spot.model';

export class Restaurant {
    public id: number;
    public restaurantName: string;
    public lunchSpots: LunchSpot[];
}
