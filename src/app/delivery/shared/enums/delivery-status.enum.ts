export enum DeliveryStatus {
    Delivered = 'DELIVERED',
    Undelivered = 'UNDELIVERED'
}
